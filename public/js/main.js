//Define Variables
const c = document.getElementById("myCanvas");
const ctx = c.getContext("2d");
// const serverUrl = '192.168.1.103'
const serverUrl = 'https://google.com'
const radius = 300;
const point_size = 4;
const center_x = parseInt(c.width / 2);
const center_y = parseInt(c.height / 2);
const font_size = "20px";


c.addEventListener('click', (event) => {
    handleCanvasClick(event.offsetX, event.offsetY)

}, false);

document.getElementById("js-stop-btn")
    .addEventListener('click', (event) => {
        handleCanvasClick(center_x, center_y)

    }, false);

function handleCanvasClick(x, y) {
    console.log({ x, y })
    initBase();
    drawCircle(x, y, 2);
    handleMovements(x, y);
}
function sendData(a, d) {
    fetch(`${serverUrl}?speed=${d}&angle=${a}`);
    console.log('sendData', a, d);
}

function handleMovements(x, y) {
    var dx = x - center_x;
    var dy = y - center_y;
    const a = parseInt(angle(dx, dy));
    const d = parseInt(distance(dx, dy));
    sendData(a, d);
}

function distance(dx, dy) {
    const d = Math.sqrt(dx * dx + dy * dy);

    return d * 4;
}

function angle(dx, dy) {
    var theta = Math.atan2(dy, dx); // range (-PI, PI]

    theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
    theta += 90; //move angle back 
    if (theta < 0) theta = 360 + theta; // range [0, 360)
    return theta;
}
function drawAxes() {
    ctx.beginPath();
    ctx.setLineDash([5, 15]);
    ctx.moveTo(0, 0);
    ctx.lineTo(c.height, c.width);

    ctx.moveTo(0, c.height);
    ctx.lineTo(c.width, 0);

    ctx.stroke();

    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.moveTo(center_x, 0);
    ctx.lineTo(center_x, c.width);

    ctx.moveTo(0, center_y);
    ctx.lineTo(c.height, center_y);

    ctx.stroke();

}

function drawCircle(x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.stroke();
}

function drawPoint(angle, distance, label) {
    var x = center_x + radius * Math.cos(-angle * Math.PI / 180) * distance;
    var y = center_y + radius * Math.sin(-angle * Math.PI / 180) * distance;

    ctx.beginPath();
    ctx.arc(x, y, point_size, 0, 2 * Math.PI);
    ctx.fill();

    ctx.font = font_size;
    ctx.fillText(label, x + 10, y);
}

function initBase() {
    ctx.clearRect(0, 0, c.width, c.height);
    drawCircle(center_x, center_y, radius);
    drawAxes();
}

initBase();
